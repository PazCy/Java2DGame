package com.paz.haviv.Objects;

public class Vector {
    private double _x;
    private double _y;

    public Vector(double x, double y)
    {
        this._x = x;
        this._y = y;
    }

    public synchronized double getX()
    {
        return this._x;
    }

    public synchronized double getY()
    {
        return this._y;
    }

    public synchronized void setX(double x)
    {
        this._x = x;
    }

    public synchronized void setY(double y)
    {
        this._y = y;
    }
}
