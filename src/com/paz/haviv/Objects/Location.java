package com.paz.haviv.Objects;

public class Location {
    private double _x;
    private double _y;

    public Location(double x, double y)
    {
        this._x = x;
        this._y = y;
    }

    public Location()
    {
        this(0, 0);
    }

    public synchronized double getX()
    {
        return this._x;
    }

    public synchronized double getY()
    {
        return this._y;
    }

    public synchronized void setX(double x)
    {
        this._x = x;
    }

    public synchronized void setY(double y)
    {
        this._y = y;
    }
}
