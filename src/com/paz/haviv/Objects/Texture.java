package com.paz.haviv.Objects;

import java.awt.*;

public class Texture {
    private Color _color;

    public Texture(Color c)
    {
        this._color = c;
    }

    public Color getColor()
    {
        return this._color;
    }
}
