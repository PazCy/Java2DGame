package com.paz.haviv.Objects;

import com.paz.haviv.Game;

import java.awt.*;

public class Tile {
    private Dimension _size;
    private Color _c;
    private Point _location;
    private TileType _type;

    public enum TileType{
        Grass,
        Stone,
        Water,
        Sand
    }

    public Tile(Color c, Point location)
    {
        this._size = new Dimension(Game.tileSize.width, Game.tileSize.height);
        this._location = location;
        this._c = c;
    }

    public Tile(TileType type, Point location)
    {
        this._size = new Dimension(Game.tileSize.width, Game.tileSize.height);
        this._location = location;
        this._type = type;
        this._c = getTextureByType(type);
    }

    public static Color getTextureByType(TileType type)
    {
        switch(type)
        {
            case Grass:
                return new Color(35, 166, 106);
            case Stone:
                return new Color(170, 170, 170);
            case Water:
                return new Color(64, 164, 223);
            case Sand:
                return new Color(249, 242, 207);
            default:
                return new Color(0, 0, 0);
        }
    }

    public synchronized Point getLocation()
    {
        return this._location;
    }

    public synchronized Color getColor()
    {
        return this._c;
    }

    public synchronized void setColor(Color c)
    {
        this._c = c;
    }

    public synchronized Dimension getSize()
    {
        return this._size;
    }

}
