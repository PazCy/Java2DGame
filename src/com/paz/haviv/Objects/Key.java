package com.paz.haviv.Objects;

public class Key {
    private int _keyCode;
    private boolean _isPressed;

    public Key(int keyCode, boolean isPressed)
    {
        this._keyCode = keyCode;
        this._isPressed = isPressed;
    }

    public synchronized void setPressed(boolean isPressed)
    {
        this._isPressed = isPressed;
    }

    public synchronized int getKeyCode()
    {
        return this._keyCode;
    }

    public synchronized boolean getPressed()
    {
        return this._isPressed;
    }
}
