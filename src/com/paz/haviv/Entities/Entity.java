package com.paz.haviv.Entities;

import com.paz.haviv.Game;
import com.paz.haviv.Logic.EngineStatics;
import com.paz.haviv.Logic.MathHelpers;
import com.paz.haviv.Objects.Location;
import com.paz.haviv.Objects.Texture;
import com.paz.haviv.Objects.Vector;
import com.paz.haviv.Render.EntityRenderer;

import java.awt.*;

public class Entity {
    private Vector _vel;
    private Location _loc;
    private Texture _texture;
    private EntityRenderer _renderer;
    protected double _speed = 0.0;
    private double _friction = 1;

    public Entity(Location location, Texture texture)
    {
        this._texture = texture;
        this._loc = location;
        this._vel = new Vector(0, 0);
        this._renderer = new EntityRenderer(this, _texture);
    }

    public Entity(Location location, Vector velocity, Texture texture)
    {
        this(location, texture);
        this._vel = velocity;
    }

    public synchronized double getSpeed()
    {
        return this._speed;
    }

    public synchronized void render(Graphics g)
    {
        this._renderer.render(g);
    }

    public synchronized void setVelocity(double x, double y)
    {
        this._vel.setX(x);
        this._vel.setY(y);
    }

    public synchronized void addVelocity(double x, double y)
    {
        double maxVelX = MathHelpers.limitValue(this._vel.getX() + x, -this._speed , this._speed);
        double maxVelY = MathHelpers.limitValue(this._vel.getY() + y, -this._speed, this._speed);
        this._vel.setX(maxVelX);
        this._vel.setY(maxVelY);
    }

    public final synchronized Vector getVelocity()
    {
        return this._vel;
    }

    double lerp(double v0, double v1, double t) {
        return (1 - t) * v0 + t * v1;
    }

    public void tick()
    {
        double newX = MathHelpers.limitValue(this._loc.getX() + (this._vel.getX() * EngineStatics.deltaTime), 0, Game.WIDTH);
        double newY = MathHelpers.limitValue(this._loc.getY() + (this._vel.getY() * EngineStatics.deltaTime), 0, Game.HEIGHT);
        //double x = lerp(this._loc.getX(), newX, 0.2);
        //double y = lerp(this._loc.getY(), newY, 0.05);
        //System.out.println(x);
        this._loc.setX(newX);
        this._loc.setY(newY);


        this._vel.setX((this._vel.getX() - (this._vel.getX() * EngineStatics.deltaTime)));
        this._vel.setY((this._vel.getY() - (this._vel.getY() * EngineStatics.deltaTime)));
    }

    public final synchronized Location getLocation()
    {
        return this._loc;
    }
}
