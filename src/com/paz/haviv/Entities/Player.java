package com.paz.haviv.Entities;

import com.paz.haviv.Objects.Location;
import com.paz.haviv.Objects.Texture;
import com.paz.haviv.Objects.Vector;

import java.awt.*;

public class Player extends Entity {

    public Player(Location location)
    {
        super(location, new Texture(Color.MAGENTA));
        super._speed = 100;
    }

    public Player(Location location, Vector velocity)
    {
        super(location, velocity, new Texture(Color.MAGENTA));
    }


}
