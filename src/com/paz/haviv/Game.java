package com.paz.haviv;

import com.paz.haviv.Entities.Player;
import com.paz.haviv.Objects.Location;
import com.paz.haviv.Render.Window;

import javax.swing.*;
import java.awt.*;

public class Game {
    public static final Dimension tileSize = new Dimension(50, 50);
    public static final Dimension mapSizeByTiles = new Dimension(200, 200);
    public static final int HEIGHT = 800;
    public static final int WIDTH = 800;
    public static final Dimension SCREEN_DIMENSIONS = new Dimension(WIDTH, HEIGHT);
    private static Player _player = null;
    private static Window _window;

    public static Point mouseLocationRelativeToScreen()
    {
        Point mouseLoc = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(mouseLoc, _window.getCanvas());
        return mouseLoc;
    }

    public static Player getPlayer()
    {
        return _player;
    }

    public Game()
    {
        this._player = new Player(new Location());
        this._window = new Window("2D GAME");
    }


}
