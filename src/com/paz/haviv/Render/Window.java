package com.paz.haviv.Render;

import com.paz.haviv.Game;
import com.paz.haviv.Logic.Input;
import com.paz.haviv.Logic.LogicHandler;
import com.paz.haviv.Objects.Key;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.VolatileImage;
import java.util.HashMap;

public class Window extends JFrame implements KeyListener {
    private BufferStrategy _buffer;
    private Canvas _canvas;
    private LogicHandler _logicHandler;
    private HashMap<Integer, com.paz.haviv.Objects.Key> _keyboardKeys = new HashMap<Integer, Key>();

    public Window(String title)
    {
        super(title);
        initWindow();
        initGraphics();
        this._logicHandler = new LogicHandler(_buffer);
        this._logicHandler.start();
        this._keyboardKeys.put(KeyEvent.VK_A, new Key(KeyEvent.VK_A, false));
        this._keyboardKeys.put(KeyEvent.VK_D, new Key(KeyEvent.VK_D, false));
        this._keyboardKeys.put(KeyEvent.VK_W, new Key(KeyEvent.VK_W,false));
        this._keyboardKeys.put(KeyEvent.VK_S, new Key(KeyEvent.VK_S, false));
    }

    private void initWindow()
    {
        getContentPane().setPreferredSize(new Dimension(Game.WIDTH, Game.HEIGHT));
        this._canvas = new Canvas();
        this._canvas.setPreferredSize(new Dimension(Game.WIDTH, Game.HEIGHT));
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(this._canvas);
        addKeyListener(this);
        pack();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                _logicHandler.stopRun();
            }
        });
        setVisible(true);
    }

    public Canvas getCanvas()
    {
        return this._canvas;
    }

    private void initGraphics()
    {
        this._canvas.createBufferStrategy(2);
        this._buffer = this._canvas.getBufferStrategy();
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e)
    {
        if(!this._keyboardKeys.containsKey(e.getKeyCode()))
            return;

        this._keyboardKeys.get(e.getKeyCode()).setPressed(true);
        Input input = new Input(Input.InputType.KeyInput, this._keyboardKeys.get(e.getKeyCode()));
        this._logicHandler.addInputEvent(input);

    }

    public void keyReleased(KeyEvent e)
    {
        if(!this._keyboardKeys.containsKey(e.getKeyCode()))
            return;

        this._keyboardKeys.get(e.getKeyCode()).setPressed(false);
        Input input = new Input(Input.InputType.KeyReleased, this._keyboardKeys.get(e.getKeyCode()));
        this._logicHandler.addInputEvent(input);
    }
}
