package com.paz.haviv.Render;

import com.paz.haviv.Entities.Entity;
import com.paz.haviv.Game;
import com.paz.haviv.Logic.EngineStatics;
import com.paz.haviv.Logic.LogicHandler;
import com.paz.haviv.Logic.MathHelpers;
import com.paz.haviv.Objects.Tile;
import com.paz.haviv.World.World;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.awt.image.BufferStrategy;
import java.nio.Buffer;
import java.util.TimerTask;

public class RenderHandler {
    private BufferStrategy _buffer;
    private LogicHandler _logic;
    private int _frameCount = 0;

    public RenderHandler(BufferStrategy buffer, LogicHandler logic) {
        this._buffer = buffer;
        this._logic = logic;

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println(_frameCount + " : " + EngineStatics.deltaTime);
                _frameCount = 0;
            }
        };
        Timer timer = new Timer();
        timer.schedule(timerTask, 0, 1000);
    }

    public synchronized void handleRender()
    {
        Graphics g = getBufferStrategy().getDrawGraphics();
        this.cleanScreen(g);
        this.draw(g);
        this.highlightMouseTile(g);
        g.dispose();
        getBufferStrategy().show();
    }

    private synchronized BufferStrategy getBufferStrategy()
    {
        return this._buffer;
    }

    private synchronized void cleanScreen(Graphics g)
    {
        g.setColor(Color.pink);
        g.fillRect(0,0, Game.WIDTH, Game.HEIGHT);
    }

    private synchronized void highlightMouseTile(Graphics g)
    {
        Point mouseLoc = Game.mouseLocationRelativeToScreen();
        int xLoc = MathHelpers.screenXToTileX(mouseLoc.x);
        int yLoc = MathHelpers.screenYToTileY(mouseLoc.y);

        Tile t = this._logic.getWorld().getTile(xLoc, yLoc);
        if(t == null)
            return;

        g.setColor(Color.cyan);
        g.drawRect(MathHelpers.tileXToScreenX(t.getLocation().x, 0), MathHelpers.tileYToScreenY(t.getLocation().y, 0), Game.tileSize.width, Game.tileSize.height);
    }

    private synchronized void draw(Graphics g)
    {
        drawWorld(g);
        drawAndUpdateEntities(g);
        this._frameCount++;
    }

    private synchronized void drawAndUpdateEntities(Graphics g)
    {
        for(Entity e : this._logic.getEntities())
        {
            e.tick();
            e.render(g);
        }
    }

    private synchronized void drawWorld(Graphics g)
    {
        World world = this._logic.getWorld();
        for(int x = MathHelpers.screenXToTileX(this._logic.getOffsetX()); x < MathHelpers.screenXToTileX(Game.WIDTH - this._logic.getOffsetX()); x++)
        {
            for (int y = MathHelpers.screenYToTileY(this._logic.getOffsetY()); y < MathHelpers.screenYToTileY(Game.HEIGHT + this._logic.getOffsetY()); y++)
            {
                Tile t = world.getTile(x, y);
                if(t == null)
                    continue;

                int mapX = MathHelpers.tileXToScreenX(x, this._logic.getOffsetX());
                int mapY = MathHelpers.tileYToScreenY(y, this._logic.getOffsetY());
                g.setColor(t.getColor());
                g.fillRect(mapX, mapY, t.getSize().width, t.getSize().height);
            }
        }
    }
}
