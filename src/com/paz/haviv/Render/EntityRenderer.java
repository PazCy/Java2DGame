package com.paz.haviv.Render;

import com.paz.haviv.Entities.Entity;
import com.paz.haviv.Objects.Texture;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class EntityRenderer {
    private Entity _entity;
    private Texture _entityTexture;

    public EntityRenderer(Entity entity, Texture entityTexture)
    {
        this._entity = entity;
        this._entityTexture = entityTexture;
    }

    public void render(Graphics g)
    {
        Rectangle2D rect = new Rectangle2D.Double(this._entity.getLocation().getX(), this._entity.getLocation().getY(), 55, 55);
        g.setColor(this._entityTexture.getColor());
        ((Graphics2D)g).fill(rect);
    }
}
