package com.paz.haviv.Logic;

public class Input {
    public enum InputType{
        KeyInput,
        KeyReleased
    }

    private Object _data;
    private InputType _type;

    public Input(InputType type, Object data)
    {
        this._data = data;
        this._type = type;
    }

    public InputType getType()
    {
        return this._type;
    }

    public Object getData()
    {
        return this._data;
    }
}
