package com.paz.haviv.Logic;

import com.paz.haviv.Entities.Entity;
import com.paz.haviv.Game;
import com.paz.haviv.Objects.Key;
import com.paz.haviv.Render.RenderHandler;
import com.paz.haviv.World.World;

import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.util.*;

public class LogicHandler extends Thread{

    private BufferStrategy _buffer;
    private boolean _shouldRun = false;
    private World _world;
    private RenderHandler _renderer;
    private int _offsetX = 0, _offsetY = 0;
    private Queue<Input> _inputQueue = new LinkedList<Input>();
    private HashSet<Entity> _entities = new HashSet<Entity>();

    public LogicHandler(BufferStrategy buffer)
    {
        this._buffer = buffer;
        this._shouldRun = true;
        this._renderer = new RenderHandler(this._buffer, this);
        this._world = new World();
        this._entities.add(Game.getPlayer());
        this.initializeGame();
    }

    public void initializeGame()
    {

    }

    public synchronized Set<Entity> getEntities()
    {
        return Collections.unmodifiableSet(_entities);
    }

    public synchronized void addInputEvent(Input input)
    {
        this._inputQueue.add(input);
    }

    public synchronized Input popInputEvent()
    {
        return this._inputQueue.poll();
    }

    public synchronized World getWorld()
    {
        return this._world;
    }

    public synchronized int getOffsetX()
    {
        return this._offsetX;
    }

    public synchronized int getOffsetY()
    {
        return this._offsetY;
    }

    public synchronized void stopRun()
    {
        this._shouldRun = false;
        yield();
    }

    public synchronized boolean getShouldRun()
    {
        return this._shouldRun;
    }

    public void run()
    {
        while(this.getShouldRun())
        {
            long _lastTime = System.nanoTime();
            tick();
            this._renderer.handleRender();

            long _current = System.nanoTime();
            EngineStatics.deltaTime = ((double)(_current - _lastTime)) / 1000000000.0;

        }
    }

    private synchronized void tick()
    {
        // Entities will tick upon drawing in order to save time and be more efficient
        handleInput();
    }

    private synchronized void handleInput()
    {
        Input input = this.popInputEvent();

        if(input == null)
            return;

        switch(input.getType())
        {
            case KeyInput:
                this.handleKeyInput((Key)input.getData());
                break;
            case KeyReleased:
                this.handleKeyReleased(((Key)input.getData()));

            default:
                break;
        }
    }
    private synchronized void handleKeyReleased(Key key)
    {
        switch(key.getKeyCode())
        {
            case KeyEvent.VK_A:
                Game.getPlayer().setVelocity(0, Game.getPlayer().getVelocity().getY());
                break;
            case KeyEvent.VK_D:
                Game.getPlayer().setVelocity(0, Game.getPlayer().getVelocity().getY());
                break;
            case KeyEvent.VK_W:
                Game.getPlayer().setVelocity(Game.getPlayer().getVelocity().getX(), 0);
                break;
            case KeyEvent.VK_S:
                Game.getPlayer().setVelocity(Game.getPlayer().getVelocity().getX(), 0);
                break;
        }
    }

    private synchronized void handleKeyInput(Key key)
    {
        if(key.getPressed()) // If the key is still pressed, push it back to the queue so we will handle that key again later
            this.addInputEvent(new Input(Input.InputType.KeyInput, key));

        switch(key.getKeyCode())
        {
            case KeyEvent.VK_A:
                Game.getPlayer().addVelocity(-Game.getPlayer().getSpeed(), 0);
                break;
            case KeyEvent.VK_D:
                Game.getPlayer().addVelocity(Game.getPlayer().getSpeed(), 0);
                break;
            case KeyEvent.VK_W:
                Game.getPlayer().addVelocity(0, -Game.getPlayer().getSpeed());
                break;
            case KeyEvent.VK_S:
                Game.getPlayer().addVelocity(0, Game.getPlayer().getSpeed());
                break;
        }
    }
}
