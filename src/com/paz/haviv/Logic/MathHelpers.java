package com.paz.haviv.Logic;

import com.paz.haviv.Game;

public class MathHelpers {

    public static int screenXToTileX(int screenX)
    {
        return screenX / Game.tileSize.width;
    }

    public static int screenYToTileY(int screenY)
    {
        return screenY / Game.tileSize.height;
    }

    public static int tileXToScreenX(int tileX, int offsetX)
    {
        return tileX * Game.tileSize.width + offsetX;
    }

    public static int limitValue(int val, int minVal, int maxVal)
    {
        return (val > maxVal) ? maxVal : (val < minVal ? minVal : val);
    }

    public static double limitValue(double val, double minVal, double maxVal)
    {
        return (val > maxVal) ? maxVal : (val < minVal ? minVal : val);
    }


    public static int tileYToScreenY(int tileY, int offsetY)
    {
        return tileY * Game.tileSize.height + offsetY;
    }
}
