package com.paz.haviv.World;

import com.paz.haviv.Game;
import com.paz.haviv.Objects.Tile;

public class World {
    private Tile[][] _tiles;

    public World()
    {
        this._tiles = new Tile[Game.mapSizeByTiles.width][Game.mapSizeByTiles.height];
        this.generate();
    }

    private void generate()
    {
        this._tiles = WorldGenerator.generateWorld(Game.mapSizeByTiles.width, Game.mapSizeByTiles.height);
    }

    public synchronized Tile getTile(int x, int y)
    {
        if(this._tiles == null || x < 0 || y < 0 || this._tiles.length <= x || this._tiles[x].length <= y )
            return null;

        return this._tiles[x][y];
    }
}
