package com.paz.haviv.World;

import com.paz.haviv.Objects.Tile;

import java.awt.*;
import java.util.Random;

public class WorldGenerator {

    public static Tile[][] generateWorld(int tileNumWidth, int tileNumHeight)
    {
        Tile[][] tiles = new Tile[tileNumWidth][tileNumHeight];
        for(int x = 0; x < tileNumWidth; x++)
        {
            for(int y = 0; y < tileNumHeight; y++)
            {
                Random rnd = new Random();
                tiles[x][y] = new Tile(Tile.TileType.values()[rnd.nextInt(Tile.TileType.values().length)], new Point(x,y));
            }
        }
        return tiles;
    }
}
